local i18n = require 'i18n'

i18n.set('en.page', 'page: %{page_number}')
i18n.set('de.page', 'Seite: %{page_number}')

local font = love.graphics.newFont('fonts/1952-RHEINMETALL.ttf', 15)
local available_languages = {
    {locale = 'en', title = 'English'},
    {locale = 'de', title = 'Deutsch'}
}
local menu = {
    x = 10,
    y = 10,
    width = 200,
    button_height = 24,
    button_margin = 2,
    items = available_languages,
    selection_index = 1
}
local page_actions = {}
local page_number = 1

local function draw_menu()
    for i = 1, #menu.items do
        local y = menu.y + (i - 1) * (menu.button_height + menu.button_margin)
        local is_selected = (i == menu.selection_index)

        if is_selected then
            love.graphics.setColor(230, 230, 230)
        else
            love.graphics.setColor(30, 30, 30)
        end

        love.graphics.rectangle('fill', menu.x, y, menu.width, menu.button_height, 5)

        love.graphics.setColor(50, 50, 50)
        love.graphics.rectangle('line', menu.x, y, menu.width, menu.button_height, 5)

        if menu.items[i].locale == i18n.getLocale() then
            local radius = 5
            local left_margin = (menu.button_height / 2) - radius
            love.graphics.setColor(160, 180, 255)
            love.graphics.ellipse('fill', menu.x + 5 + left_margin, y + menu.button_height / 2, radius, radius)
            love.graphics.setColor(120, 140, 255)
            love.graphics.ellipse('line', menu.x + 5 + left_margin, y + menu.button_height / 2, radius, radius)
        end

        if is_selected then
            love.graphics.setColor(30, 30, 30)
        else
            love.graphics.setColor(230, 230, 230)
        end

        local button_title = menu.items[i].title
        local text_y = y + (menu.button_height - font:getHeight()) / 2
        love.graphics.printf(button_title, menu.x, text_y, menu.width, 'center')
    end
end

local function load_pages()
    for _, language in pairs(available_languages) do
        local pages = require('pages.' .. language.locale)
        for page_index, page in ipairs(pages) do
            if page_actions[page_index] == nil then
                page_actions[page_index] = {}
            end

            local page_key = language.locale .. '.page_' .. page_index
            i18n.set(page_key .. '.text', page.text)
            for action_key, action_text in pairs(page.actions) do
                page_actions[page_index][action_key] = true
                i18n.set(page_key .. '.actions.' .. action_key, action_text)
            end
        end
    end
end

local function previous_page()
    page_number = page_number - 1
end

local function next_page()
    page_number = page_number + 1
end

local function select_menu_item(new_index)
    menu.selection_index = (new_index + 1) % #menu.items + 1
end

function love.load()
    load_pages()
    love.graphics.setFont(font)
end

function love.draw()
    draw_menu()
    love.graphics.setColor(255, 255, 255)
    love.graphics.print(i18n('page', {page_number = page_number}), 700, 550)

    local page_key = 'page_' .. page_number
    local page_text = i18n(page_key .. '.text')

    if page_text == nil then
        page_text = 'Missing text for page ' .. page_number .. '!'
    end

    love.graphics.printf(page_text, 400, 10, 400)

    local actions = page_actions[page_number]
    if actions then
        local y = 300
        for action_key, _ in pairs(actions) do
            local action_text = i18n(page_key .. '.actions.' .. action_key)

            if action_text == nil then
                action_text = 'Missing text for action ' .. action_key .. '!'
            end

            love.graphics.print(action_text, 400, y)
            y = y + 20
        end
    end
end

function love.keyreleased(key)
    if key == 'left' then
        previous_page()
    elseif key == 'right' then
        next_page()
    elseif key == 'up' then
        select_menu_item(menu.selection_index - 1)
    elseif key == 'down' then
        select_menu_item(menu.selection_index + 1)
    elseif key == 'return' then
        i18n.setLocale(menu.items[menu.selection_index].locale)
    end
end

function love.gamepadpressed(_, button)
    if string.sub(button, 1, 2) == 'dp' then
        love.keyreleased(string.sub(button, 3))
    elseif button == 'a' then
        love.keyreleased('return')
    end
end
