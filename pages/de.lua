return {
    [1] = {
        text = 'Das ist Seite 1. Hier befindet sich der Anfang der Geschichte.',
        actions = {
            open_door = 'Gehe durch die Tür'
        }
    },
    [2] = {
        text = 'Hinter der Tür ist ein Raum. Aber das ist eine andere Geschichte.',
        actions = {
            go_back = 'Gehe zurück',
            next_door = 'Gehe durch die nächste Tür'
        }
    }
}
